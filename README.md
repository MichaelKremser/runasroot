# runasroot

This repository contains scripts for executing certain programs as root if the user has proper permissions to do so.

# Installation

## Operating Systems Using APT (Debian, Ubuntu...)

Download the package, for example using wget:

```
wget https://gitlab.com/MichaelKremser/runasroot/-/jobs/4650991922/artifacts/file/build/runasroot_0.2_all.deb
```

Then install the package:

```
sudo dpkg -i runasroot_0.2_all.deb
```

For your convenience you can assign a keyboard shortcut.